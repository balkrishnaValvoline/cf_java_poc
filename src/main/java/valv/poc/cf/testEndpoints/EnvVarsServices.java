package valv.poc.cf.testEndpoints;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sap.xsa.security.container.XSUserInfo;

import valv.poc.cf.environment.EnvironmentVariableAccessor;


/**
 * This is Servlet Class to test the functionalities of the POC
 * @author Balkrishna.Meena
 *
 */
@Path("/svc")
public class EnvVarsServices {
	
	private static final Logger logger = LoggerFactory.getLogger(EnvVarsServices.class);
	
	
	@Inject
	private EnvironmentVariableAccessor envAccessor;
	
	@GET
	@Path("/test")
	public String testSvc(@Context HttpServletRequest request) {
		logger.info("[ENTER] test method");
		logger.debug("Test method running");
		return "Service Running";
	}
	
	
	@GET
	@Path("/testHttpConnectionOnDocker")
	@Produces(MediaType.APPLICATION_JSON)
	public Response testHttp() {
		URL northWindUrl = null;
		HttpURLConnection connection = null;
		Object resp = null;
		int status = 500;
		try {
			northWindUrl = new URL("https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$top=10&$format=json");
			connection = (HttpURLConnection) northWindUrl.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Content-Type", "application/json; charset=utf8");
			connection.getContentType();
			status = connection.getResponseCode();
			if(status == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(
						connection.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				resp = response.toString();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return Response.status(status).entity(resp).build();
	}
	
	@GET
	@Path("/dbConnectionTest")
	public String dbConnectionTest() {
		String connectionString = "jdbc:sap://zeus.hana.prod.us-east-1.whitney.dbaas.ondemand.com:22762?encrypt=true&validateCertificate=true&currentschema=DASH_POC";
		String user = "DASH_POC_70UTJOIXONAM3MTWTIYJQOGYT_DT";
		String password = "Ga0ixBOv9_DlxDgDTN_u9YVHA1BhDopE2rIArZIBYz-PBuKk_8iDNstTNt2nolkV2bJdXkaAC-zKTQiBXfzLLGmxyt3J.P939JlPVXSjrq0VtYY8xy0PzCMoreUnZaGU";
		
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(connectionString, user, password);
		} catch (SQLException e) {
			return "Connection Failed. User/Passwd Error? Message: " + e.getMessage();
		}
		if (connection != null) {
			return "Connection to HANA successful!";
		}else {
			return "Failed";
		}
	}
	
	@GET
	@Path("/xsToken")
	public String getToken(@Context HttpServletRequest request) {
		XSUserInfo user = (XSUserInfo) request.getUserPrincipal();
		return user.getAppToken();
	}
	
	
	@GET
	@Path("/allEnvVars")
	@Produces(MediaType.APPLICATION_JSON)
	public String getEnvVariablesString() {
		JSONObject retObj = envAccessor.getEnvironmentVariables();
		return retObj.toString();
	}
}
