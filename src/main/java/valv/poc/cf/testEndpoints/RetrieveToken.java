package valv.poc.cf.testEndpoints;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sap.xsa.security.container.XSUserInfo;
import com.sap.xsa.security.container.XSUserInfoException;

/**
 * Returns Logged In User's Details along with JWT Token supplied by App-Router
 * @author Balkrishna.Meena
 *
 */
@WebServlet("/api/RetrieveToken")
public class RetrieveToken extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		XSUserInfo userInfo = (XSUserInfo) request.getUserPrincipal();
		try {
			response.getWriter().append("Client ID: ").append("" + userInfo.getClientId());
			response.getWriter().append("\n");
			response.getWriter().append("Email: ").append("" + userInfo.getEmail());
			response.getWriter().append("\n");
			response.getWriter().append("Family Name: ").append("" + userInfo.getFamilyName());
			response.getWriter().append("\n");
			response.getWriter().append("First Name: ").append("" + userInfo.getGivenName());
			response.getWriter().append("\n");
			response.getWriter().append("OAuth Grant Type: ").append("" + userInfo.getGrantType());
			response.getWriter().append("\n");
			response.getWriter().append("OAuth Token: ").append("" + userInfo.getAppToken());
			response.getWriter().append("\n");

		} catch (XSUserInfoException e) {
			e.printStackTrace(response.getWriter());
		}
	}

}
