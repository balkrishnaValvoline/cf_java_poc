package valv.poc.cf.destinations;

import java.io.InputStream;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import valv.poc.cf.environment.SharedConstants;

/**
 * Connection attributes for connecting to back end systems.
 */
public class ConnectionAttributes {

	public enum ProxyType {
		INTERNET, ONPREMISE;

		public static ProxyType fromString(String text) {
			return ProxyType.valueOf(text.toUpperCase());
		}
	}

	public enum AuthenticationType {
		NO_AUTHENTICATION("NoAuthentication"), BASIC_AUTHENTICATION("BasicAuthentication"), PRINCIPAL_PROPAGATION(
				"PrincipalPropagation"), SAMLBEARER_AUTHENTICATION("OAuth2SAMLBearerAssertion");

		private String name;

		private AuthenticationType(String name) {
			this.name = name;
		}

		public static AuthenticationType fromString(String text) {
			for (AuthenticationType authType : AuthenticationType.values()) {
				if (authType.name.equalsIgnoreCase(text)) {
					return authType;
				}
			}
			return null;

		}

		@Override
		public String toString() {
			return name;
		}
	}
	
	private ProxyType proxyType;
	private AuthenticationType authenticationType;
	private String authToken;
	private DestinationConfiguration destConfig;

	private ConnectionAttributes() {
	}
	
	private static final String PARAM_DESTINATION_CONFIG = "destinationConfiguration";
	private static final String PARAM_PROXY_TYPE = "ProxyType";
	private static final String PARAM_AUTHENTICATION_TYPE = "Authentication";
	private static final String PARAM_TOKEN_ARRAY = "authTokens";
	private static final String PARAM_TOKEN_VALUE = "value";
	private static final String PARAM_TOKEN_TYPE = "type";

	public ProxyType getProxyType() {
		return proxyType;
	}

	public AuthenticationType getAuthenticationType() {
		return authenticationType;
	}

	public String getAuthenticationToken() {
		return authToken;
	}

	public DestinationConfiguration getDestConfig() {
		return destConfig;
	}
	
	/**
	 * Method to generate {@link ConnectionAttributes} from response received from CF Destination Service 
	 * @param destinationJson
	 * @return
	 */
	public static ConnectionAttributes fromDestination(InputStream destinationJson) {
		JSONObject destination = new JSONObject(new JSONTokener(destinationJson));
		JSONObject destinationConfiguration = destination.getJSONObject(PARAM_DESTINATION_CONFIG);
		DestinationConfiguration destConfig = new DestinationConfiguration(destinationConfiguration);
		ConnectionAttributes attributes = new ConnectionAttributes();
		attributes.destConfig = destConfig;
		attributes.proxyType = ProxyType.fromString(destinationConfiguration.optString(PARAM_PROXY_TYPE));
		attributes.authenticationType = AuthenticationType
				.fromString(destinationConfiguration.optString(PARAM_AUTHENTICATION_TYPE));
		
		if (ifUseAuthTokens(attributes)) {
			JSONArray tokensArr = destination.getJSONArray(PARAM_TOKEN_ARRAY);

			for (int i = 0; i < tokensArr.length(); i++) {
				JSONObject token = tokensArr.getJSONObject(i);
				String tokenType = token.getString(PARAM_TOKEN_TYPE);

				if (isValidAuthorization(tokenType)) {
					attributes.authToken = token.getString(PARAM_TOKEN_VALUE);
					break;
				}
			}
		}
		return attributes;
	}
	
	private static boolean isValidAuthorization(String tokenType) {
		return tokenType.equals(SharedConstants.BASIC_WITH_TRAILING_SPACE.trim()) || tokenType.equals(SharedConstants.BEARER_WITH_TRAILING_SPACE.trim());
	}

	private static boolean ifUseAuthTokens(ConnectionAttributes attributes) {
		return attributes.authenticationType == AuthenticationType.BASIC_AUTHENTICATION || attributes.authenticationType == AuthenticationType.SAMLBEARER_AUTHENTICATION;
	}
	
}
