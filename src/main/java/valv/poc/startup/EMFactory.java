package valv.poc.startup;

import java.sql.SQLException;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public final class EMFactory {
	
	public static final String PERSISTENCE_UNIT_NAME = "cf_poc_db_pu";

	/**
	 * The static {@link EntityManagerFactory}.
	 */
	private static EntityManagerFactory entityManagerFactory;

	/**
	 * Returns the singleton EntityManagerFactory instance for accessing the
	 * default database.
	 * 
	 * @return the singleton EntityManagerFactory instance
	 * @throws NamingException
	 *             if a naming exception occurs during initialization
	 * @throws SQLException
	 *             if a database occurs during initialization
	 */
	public static synchronized EntityManagerFactory getEntityManagerFactory() throws NamingException, SQLException {
		if (EMFactory.entityManagerFactory == null) {
			EMFactory.entityManagerFactory =
				Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		}
		return EMFactory.entityManagerFactory;
	}

}
