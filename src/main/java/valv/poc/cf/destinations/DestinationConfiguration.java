package valv.poc.cf.destinations;

import org.json.JSONObject;

import valv.poc.cf.destinations.ConnectionAttributes.AuthenticationType;
import valv.poc.cf.destinations.ConnectionAttributes.ProxyType;

public class DestinationConfiguration {

	private String name;
	private String type;
	private String url;
	private AuthenticationType authentication;
	private ProxyType proxyType;
	private String user;
	private String password;

	public DestinationConfiguration(JSONObject destObj) {
		this.name = destObj.has("Name")?destObj.getString("Name"):"";
		this.type = destObj.has("Type")?destObj.getString("Type"):"";
		this.url = destObj.has("URL")?destObj.getString("URL"):"";
		this.authentication = AuthenticationType.fromString(destObj.get("Authentication").toString());
		this.proxyType = ProxyType.fromString(destObj.getString("ProxyType"));
		this.user = destObj.has("User")?destObj.getString("User"):"";
		this.password = destObj.has("Password")?destObj.getString("Password"):"";
	}

	public String getName() {
		return name;
	}


	public String getType() {
		return type;
	}


	public String getUrl() {
		return url;
	}


	public AuthenticationType getAuthentication() {
		return authentication;
	}


	public ProxyType getProxyType() {
		return proxyType;
	}


	public String getUser() {
		return user;
	}


	public String getPassword() {
		return password;
	}


	public JSONObject toJSONObject(DestinationConfiguration destConfig) {
		JSONObject destObj = new JSONObject();
		destObj.put("Name", destConfig.getName());
		destObj.put("Type", destConfig.getType());
		destObj.put("URL", destConfig.getUrl());
		destObj.put("Authentication", destConfig.getAuthentication().toString());
		destObj.put("ProxyType", destConfig.getProxyType());
		destObj.put("User", destConfig.getUser());
		destObj.put("Password", destConfig.getPassword());
		return destObj;
	}

}
