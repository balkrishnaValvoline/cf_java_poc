package valv.cf.poc.rfc;

import javax.enterprise.context.RequestScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sap.conn.jco.AbapException;
import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.JCoException;
import com.sap.conn.jco.JCoField;
import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoParameterFieldIterator;
import com.sap.conn.jco.JCoParameterList;
import com.sap.conn.jco.JCoRepository;

import valv.poc.cf.destinations.DestinationAccessor;

public class RFCTest {
	private static final Logger logger = LoggerFactory.getLogger(RFCTest.class);

	public RFCTest() {
		// TODO Auto-generated constructor stub
	}

	public JsonObject rfcPingTest(String userName) {
		JsonObject outputObj = new JsonObject();
		System.out.println("Inside RFC Bala Test");
		try {
			// access the RFC Destination "JCoDemoSystem"
			JCoDestination destination = JCoDestinationManager.getDestination("VDH_RFC");
			// make an invocation of Function in the backend;
			JCoRepository repo = destination.getRepository();
			logger.info("repository : ", repo.getName());
			JCoFunction func = repo.getFunction("RHXSS_SER_GET_EMPLOYEE_DATA");
			if(userName !=null && !userName.equals("")) {
				JCoParameterList importParam = func.getImportParameterList();
				importParam.setValue("USERNAME", userName);
			}
			func.execute(destination);
			JCoParameterList exportParamList = func.getExportParameterList();
			JCoParameterFieldIterator iter = exportParamList.getParameterFieldIterator();
			JsonArray jarr = new JsonArray();
			while (iter.hasNextField()) {
				JCoField jcoField = iter.nextField();
				JsonObject obj = new JsonObject();
				obj.addProperty(jcoField.getName(), jcoField.getValue()!=null? jcoField.getValue().toString():"");
				jarr.add(obj);
			}
			outputObj.add("rfcOut", jarr);
			// respOut = "Repository Name: " + repo.getName();
		} catch (AbapException ae) {
			System.out.println("ABAP Exception");
			ae.printStackTrace();
			outputObj.addProperty("Error", ae.getMessage());
		} catch (JCoException e) {
			System.out.println("JCO Exception");
			e.printStackTrace();
			outputObj.addProperty("Error", e.getMessage());
		}
		return outputObj;
	}

	public JsonObject rfcCallPrincipalPropagation(String userName) {
		JsonObject outputObj = new JsonObject();
		System.out.println("Inside RFC SNC Test");
		try {
			// access the RFC Destination "JCoDemoSystem"
			JCoDestination destination = JCoDestinationManager.getDestination("VDH_RFC_SNC");
			// make an invocation of Function in the backend;
			JCoRepository repo = destination.getRepository();
			logger.info("repository : ", repo.getName());
			JCoFunction func = repo.getFunction("RHXSS_SER_GET_EMPLOYEE_DATA");
			if(userName !=null && !userName.equals("")) {
				JCoParameterList importParam = func.getImportParameterList();
				importParam.setValue("USERNAME", userName);
			}
			func.execute(destination);
			JCoParameterList exportParamList = func.getExportParameterList();
			JCoParameterFieldIterator iter = exportParamList.getParameterFieldIterator();
			JsonArray jarr = new JsonArray();
			while (iter.hasNextField()) {
				JCoField jcoField = iter.nextField();
				JsonObject obj = new JsonObject();
				obj.addProperty(jcoField.getName(), jcoField.getValue()!=null? jcoField.getValue().toString():"");
				jarr.add(obj);
			}
			outputObj.add("rfcOut", jarr);
			// respOut = "Repository Name: " + repo.getName();
		} catch (AbapException ae) {
			System.out.println("ABAP Exception");
			ae.printStackTrace();
			outputObj.addProperty("Error", ae.getMessage());
		} catch (JCoException e) {
			System.out.println("JCO Exception");
			e.printStackTrace();
			outputObj.addProperty("Error", e.getMessage());
		}
		return outputObj;
	}

}
