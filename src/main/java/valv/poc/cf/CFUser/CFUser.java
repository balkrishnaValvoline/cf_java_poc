package valv.poc.cf.CFUser;

import java.util.Arrays;

/**
 * The CFUser class represents Cloud Foundry User.
 * The user having an active session in the application.
 * @author Balkrishna.Meena
 *
 */
public class CFUser {
	
	private String givenName;
	private String lastName;
	private String UserName;
	private String[] Role;
	private String frID;
	private String userType;
	private String mail;
	
	public CFUser(String givenName, String lastName, String UserName, String[] Role, String frID, String userType, String mail) {
		this.givenName = givenName;
		this.lastName = lastName;
		this.UserName = UserName;
		this.Role = Role;
		this.frID = frID;
		this.userType = userType;
		this.mail = mail;
	}
	
	public String getGivenName() {
		return givenName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getUserName() {
		return UserName;
	}
	public String[] getRole() {
		return Role;
	}
	public String getFrID() {
		return frID;
	}
	public String getUserType() {
		return userType;
	}
	public String getMail() {
		return mail;
	}
	
	@Override
	public String toString() {
		return "CFUSER [givenName = " + this.givenName
				+ ", lastName = "
				+ this.lastName
				+ ", UserName = "
				+ this.UserName 
				+ ", Role = "
				+ Arrays.toString(this.Role)
				+ ", frID = "
				+ this.frID
				+ ", userType = "
				+ this.userType
				+ ", mail = "
				+ this.mail;
	}
	
	
}
