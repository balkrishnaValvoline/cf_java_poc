package valv.cf.poc.persistence;

import java.sql.SQLException;
import java.util.List;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.json.JSONArray;
import org.json.JSONObject;

import valv.poc.cf.persistence.model.EmplInfo;
import valv.poc.startup.EMFactory;

public class EmplInfoDAO {

	public JSONArray getEmplInfo() {
		JSONArray arr = null;
		try {
			EntityManager em = EMFactory.getEntityManagerFactory().createEntityManager();
			TypedQuery<EmplInfo> tq = em.createNamedQuery("EmplInfo.findAll", EmplInfo.class);
			List<EmplInfo> results = tq.getResultList();
			arr = new JSONArray();
			for (EmplInfo empl : results) {
				JSONObject obj = new JSONObject();
				obj.put("EMPLID", empl.getEmplId());
				obj.put("NAME", empl.getEmplName());
				arr.put(obj);
			}
			return arr;
		} catch (NamingException | SQLException e) {
			e.printStackTrace();
		}
		return arr;
	}
}
