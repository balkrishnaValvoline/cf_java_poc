package valv.poc.cf.authentication;


import javax.enterprise.context.RequestScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sap.cloud.security.xsuaa.client.ClientCredentials;
import com.sap.cloud.security.xsuaa.client.DefaultOAuth2TokenService;
import com.sap.cloud.security.xsuaa.client.OAuth2TokenResponse;
import com.sap.cloud.security.xsuaa.client.XsuaaDefaultEndpoints;
import com.sap.cloud.security.xsuaa.tokenflows.ClientCredentialsTokenFlow;
import com.sap.cloud.security.xsuaa.tokenflows.TokenFlowException;
import com.sap.cloud.security.xsuaa.tokenflows.XsuaaTokenFlows;

@RequestScoped
public class TokenFactory {
	private static final Logger LOGGER = LoggerFactory.getLogger(TokenFactory.class);
	
	public class GetTokenException extends Exception{
		private static final long serialVersionUID = 1924287017301950182L;

		private GetTokenException(String msg, Exception e) {
			super(msg, e);
		}
	}

	public String getJWTWithClientCredentials(XsuaaDefaultEndpoints xsUaaEndpoint, ClientCredentials clientCred) {
		LOGGER.info("[ENTER] TokenFactory - getJWTWithClientCredentials Method");
		XsuaaTokenFlows tokenFlow = new XsuaaTokenFlows(new DefaultOAuth2TokenService(), xsUaaEndpoint, clientCred);
		ClientCredentialsTokenFlow clientTokenFlow = tokenFlow.clientCredentialsTokenFlow();
		try {
			OAuth2TokenResponse oAuthResp = clientTokenFlow.execute();
			return oAuthResp.getAccessToken();
		} catch (IllegalArgumentException | TokenFlowException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
