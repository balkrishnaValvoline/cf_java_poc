package valv.poc.cf.CFUser;

import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Iterator;

import javax.enterprise.context.RequestScoped;

import org.json.JSONArray;
import org.json.JSONObject;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.sap.xsa.security.container.XSUserInfo;
import com.sap.xsa.security.container.XSUserInfoException;

/**
 * Class to generate {@link CFUser} from Http Session
 * 
 * @author Balkrishna.Meena
 *
 */

@RequestScoped
public class CFUserAccessor {

	/**
	 * Returns CFUser. JWT token is decoded to get the user details
	 * 
	 * @param userInfo
	 * @return {@link CFUser}
	 * @throws XSUserInfoException
	 */
	public CFUser getUserFromSession(XSUserInfo userInfo) throws XSUserInfoException {
		if (userInfo != null) {
			String accessToken = userInfo.getAppToken();
			if (accessToken != null && !accessToken.equals("")) {
				DecodedJWT jwt = null;
				try {
					jwt = JWT.decode(accessToken);
					/*
					 * Get Payload data from jwt token - payload is Base64 string which needs to be
					 * decoded
					 */
					String payload = jwt.getPayload();
					Decoder base64Decoder = Base64.getDecoder();
					String decodedPayload = new String(base64Decoder.decode(payload));
					JSONObject samlAttributes = getSAMLAttributes(decodedPayload);
					CFUser user = createCFUserFromSamlAttributes(samlAttributes);
					return user;
				} catch (JWTDecodeException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}

	/**
	 * DASH specific method. SAML attributes are used to create CFUser
	 * 
	 * @param samlAttributes
	 * @return {@link CFUser}
	 */
	private CFUser createCFUserFromSamlAttributes(JSONObject samlAttributes) {
		JSONArray rolesArray = samlAttributes.getJSONArray("Role");
		// Extract the role name from SAML. Ex:
		// cn=PRDFND,ou=groups,dc=valvoline,dc=com
		String[] arRoles = new String[rolesArray.length()];
		for (int i = 0; i < rolesArray.length(); i++) {
			String[] splitCommas = rolesArray.get(i).toString().split(",");
			if (splitCommas[0] != null) {
				String[] splitEquals = splitCommas[0].split("=");
				if (splitEquals.length > 1) {
					arRoles[i] = splitEquals[1];
				}
			}
		}
		CFUser cfUser = new CFUser(samlAttributes.getString("givenName"), samlAttributes.getString("lastName"),
				samlAttributes.getString("UserName"), arRoles, samlAttributes.getString("frID"),
				samlAttributes.getString("userType"), samlAttributes.getString("mail"));
		return cfUser;

	}

	/**
	 * Creates {@link JSONObject} containing SAML assertion attributes
	 * 
	 * @param decodedPayload
	 * @return {@link JSONObject}
	 */
	private JSONObject getSAMLAttributes(String decodedPayload) {
		JSONObject payload = new JSONObject(decodedPayload);
		JSONObject xsUserAttributes = payload.getJSONObject("xs.user.attributes");
		Iterator<String> xsAttrIterator = xsUserAttributes.keys();
		// Get keys in user attributes
		JSONObject samlAttributes = new JSONObject();
		while (xsAttrIterator.hasNext()) {
			String samlKey = xsAttrIterator.next();
			if (xsUserAttributes.getJSONArray(samlKey).length() == 1) {
				samlAttributes.put(samlKey, xsUserAttributes.getJSONArray(samlKey).getString(0));
			} else {
				samlAttributes.put(samlKey, xsUserAttributes.getJSONArray(samlKey));
			}
		}
		return samlAttributes;

	}

}
