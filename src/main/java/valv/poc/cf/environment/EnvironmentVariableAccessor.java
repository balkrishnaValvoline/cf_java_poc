package valv.poc.cf.environment;
import java.text.MessageFormat;
import java.util.Iterator;
import javax.enterprise.context.RequestScoped;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


/**
 * This class is responsible for accessing Environment Variables of the running JAVA application
 * @author Balkrishna.Meena
 *
 */
@RequestScoped
public class EnvironmentVariableAccessor {
	
	private static final String VCAP_SERVICES = System.getenv("VCAP_SERVICES");
	private static final String VCAP_SERVICES_CREDENTIALS = "credentials";
	private static final String VCAP_SERVICES_NAME = "name";
	
	public EnvironmentVariableAccessor() {
	}
	
	/**
	 * Returns {@link JSONObject} for the given Environment Variable Name
	 * @param envVariable
	 * @param instanceName
	 * @return {@link JSONObject}
	 */
	public JsonObject getEnvironmentVariableByName(String envVariable, String instanceName) {
		JsonObject envVar = null;
		if (VCAP_SERVICES !=null) {
			envVar = JsonParser.parseString(VCAP_SERVICES).getAsJsonObject();
			if (envVar.has(envVariable) && envVar.isJsonArray() && instanceName != null && !instanceName.equals("")) {
				Iterator<JsonElement> envIterator = envVar.getAsJsonArray().iterator();
				while (envIterator.hasNext()) {
					JsonObject envObj = envIterator.next().getAsJsonObject();
					if (envObj.get("instance_name").toString().equals(instanceName)) {
						return envObj;
					}
				}
			}
		}
		return envVar;
	}
	
	/**
	 * Test Method to return show all the environment variables
	 * @return {@link JSONObject}
	 */
	public JSONObject getEnvironmentVariables() {
		JSONObject envVar = null;
		if(VCAP_SERVICES !=null) {
			envVar = new JSONObject(VCAP_SERVICES);
		}else {
			envVar = new JSONObject();
			envVar.put("msg", "No Env Var Found");
		}
		return envVar;
	}
	
	/**
	 * Returns service credentials for a given service from VCAP_SERVICES
	 * @param serviceName
	 * @return
	 * @throws JSONException
	 */
	public static JSONObject getServiceCredentials(String serviceName) throws JSONException {
		JSONObject jsonObj = new JSONObject(VCAP_SERVICES);
		JSONArray jsonArr = jsonObj.getJSONArray(serviceName);
		return jsonArr.getJSONObject(0).getJSONObject(VCAP_SERVICES_CREDENTIALS);
	}
	
	/**
	 * Return Service Credentials for a given service instance
	 * @param serviceName
	 * @param serviceInstanceName
	 * @return {@link JSONObject}
	 * @throws JSONException
	 */
	public static JSONObject getServiceCredentials(String serviceName, String serviceInstanceName)
			throws JSONException {
		JSONObject jsonObj = new JSONObject(VCAP_SERVICES);
		JSONArray jsonarr = jsonObj.getJSONArray(serviceName);
		for (int i = 0; i < jsonarr.length(); i++) {
			JSONObject serviceInstanceObject = jsonarr.getJSONObject(i);
			String instanceName = serviceInstanceObject.getString(VCAP_SERVICES_NAME);
			if (instanceName.equals(serviceInstanceName)) {
				return serviceInstanceObject.getJSONObject(VCAP_SERVICES_CREDENTIALS);
			}
		}
		throw new RuntimeException(MessageFormat.format("Service instance {0} of service {1} not bound to application",
				serviceInstanceName, serviceName));
	}
	
	/**
	 * Returns service credentials attribute for a given service from VCAP_SERVICES
	 * @param serviceName
	 * @param attributeName
	 * @return
	 * @throws JSONException
	 */
	public static String getServiceCredentialsAttribute(String serviceName, String attributeName) throws JSONException {
		JSONObject jsonCredentials = getServiceCredentials(serviceName);
		return jsonCredentials.getString(attributeName);
	}
	
	/**
	 * Returns service credentials attribute for a given service from VCAP_SERVICES
	 * @param serviceName
	 * @param serviceInstanceName
	 * @param attributeName
	 * @return
	 * @throws JSONException
	 */
	public static String getServiceCredentialsAttribute(String serviceName, String serviceInstanceName,
			String attributeName) throws JSONException {
		JSONObject jsonCredentials = getServiceCredentials(serviceName, serviceInstanceName);
		return jsonCredentials.getString(attributeName);
	}
}
