package valv.poc.cf.persistence.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: EmplInfo
 *
 */
@Entity
@Table(schema="DASH_POC",name="EMPLINFO")
@NamedQuery(name="EmplInfo.findAll",query="Select e FROM EmplInfo e")
public class EmplInfo implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="EMPLID")
	private Integer emplId;
	
	@Column(name="NAME")
	private String emplName;

	public Integer getEmplId() {
		return emplId;
	}

	public void setEmplId(Integer emplId) {
		this.emplId = emplId;
	}

	public String getEmplName() {
		return emplName;
	}

	public void setEmplName(String emplName) {
		this.emplName = emplName;
	}
	
}
