package valv.poc.cf.testEndpoints;

import static java.net.HttpURLConnection.HTTP_OK;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.google.gson.JsonObject;
import com.sap.xsa.security.container.XSUserInfo;
import com.sap.xsa.security.container.XSUserInfoException;

import valv.cf.poc.persistence.EmplInfoDAO;
import valv.cf.poc.rfc.RFCTest;
import valv.poc.cf.CFUser.CFUser;
import valv.poc.cf.CFUser.CFUserAccessor;
import valv.poc.cf.destinations.ConnectionAttributes;
import valv.poc.cf.destinations.ConnectionAttributes.AuthenticationType;
import valv.poc.cf.destinations.DestinationAccessor;
import valv.poc.cf.destinations.DestinationConfiguration;
import valv.poc.cf.environment.SharedConstants;
import valv.poc.cf.http.HttpResponse;
import valv.poc.cf.http.HttpUtils;

@Path("/api")
public class TestApis {
	

	@GET
	@Path("/rfcPingTechUser")
	public Response rfcTestBala(@QueryParam("userName") String userName) {
		RFCTest rfcTestDI = new RFCTest();
		JsonObject response = rfcTestDI.rfcPingTest(userName);
		return Response.ok(response.toString()).build();
	}
	
	@GET
	@Path("/rfcEmplDataPP")
	public Response rfcPrincipalPropagationTest(@QueryParam("userName") String userName) {
		RFCTest rfcTestDI = new RFCTest();
		JsonObject response = rfcTestDI.rfcCallPrincipalPropagation(userName);
		return Response.ok(response.toString()).build();
	}
	
	@Inject
	private CFUserAccessor userAccessor;
	
	/**
	 * Returns Logged In User Details
	 * @param request
	 * @return
	 */
	@GET
	@Path("/user")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCurrentUser(@Context HttpServletRequest request) {
		try {
			XSUserInfo userInfo = (XSUserInfo) request.getUserPrincipal();
			CFUser user = userAccessor.getUserFromSession(userInfo);
			return Response.ok().entity(user.toString()).build();
		} catch (XSUserInfoException e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
	}
	
	
	@Inject
	private DestinationAccessor destAccessor;
	/**
	 * Method to test destinations at Cloud Foundry
	 * @param destName
	 * @return
	 */
	@GET
	@Path("/testDestination/{destName}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response testDestination(@PathParam("destName") String destName) {
		ConnectionAttributes destination = destAccessor.getDestinationAtSpaceByName(destName);
		JSONObject data = this.testDestination(destination,destName);
		return Response.ok().entity(data.toString()).build();
	}
	
	
	@GET
	@Path("/jpaTest")
	public Response jpaConnectionTest() {
		EmplInfoDAO emplInfoDao = new EmplInfoDAO();
		JSONArray resp = emplInfoDao.getEmplInfo();
		return Response.ok(resp.toString()).build();
	}
	
	
	/**
	 * Helper method to make Http call and get response from destination
	 * @param destination
	 * @param destName
	 * @return
	 */
	private JSONObject testDestination(ConnectionAttributes destination, String destName) {
		DestinationConfiguration destConfig = destination.getDestConfig();
		try {
			String destUrl = null;
			if(destName.equals("North_Wind_OData")) {
				destUrl = destConfig.getUrl() + "/" + "V4/Northwind/Northwind.svc/Customers?$top=10&$format=json";
			}else {
				destUrl = destConfig.getUrl() + "/" + "dash_testing/services/order.xsodata/ProductList?$top=10&$format=json";
			}
				
			HttpURLConnection httpClient = HttpUtils.openUrlConnection(new URL(destUrl));
			httpClient.setRequestMethod("GET");
			if(destination.getAuthenticationType().compareTo(AuthenticationType.NO_AUTHENTICATION) != 0) {
				httpClient.setRequestProperty(SharedConstants.HEADER_AUTORIZATION, SharedConstants.BASIC_WITH_TRAILING_SPACE + destination.getAuthenticationToken());
			}
			
			HttpResponse response = HttpUtils.getResponse(httpClient);

			if (response.getResponseCode() == HTTP_OK) {
				JSONObject data = new JSONObject(new JSONTokener(response.getResponseStream()));
				return data;
			}else {
				return new JSONObject(new JSONTokener(response.getResponseStream()));
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
