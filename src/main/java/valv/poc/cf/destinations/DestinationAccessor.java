package valv.poc.cf.destinations;

import static java.net.HttpURLConnection.HTTP_NOT_FOUND;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sap.cloud.security.xsuaa.client.ClientCredentials;
import com.sap.cloud.security.xsuaa.client.XsuaaDefaultEndpoints;

import valv.poc.cf.authentication.TokenFactory;
import valv.poc.cf.environment.EnvironmentVariableAccessor;
import valv.poc.cf.environment.SharedConstants;
import valv.poc.cf.exceptions.DestinationNotFoundException;
import valv.poc.cf.http.HttpResponse;
import valv.poc.cf.http.HttpUtils;

@RequestScoped
public class DestinationAccessor {

	private static final Logger LOGGER = LoggerFactory.getLogger(DestinationAccessor.class);

	private static final String DESTINATON_XSUAA_SERVICE_NAME = "xsuaa";
	private static final String DESTINATON_XSUAA_INSTANCE = "b-xsuaa";
	private static final String DESTINATION_SERVICE_PROPTY_URI = "uri";
	private static final String DESTINATION_SERVICE_NAME = "destination";
	private static final String DESTINATION_INSTANCE = "b-destinations";
	private static final String DESTINATION_SERVICE_PATH = "/destination-configuration/v1/destinations/%s";

	public DestinationAccessor() {
	}

	@Inject
	private TokenFactory tokenFactory;

	/**
	 * Returns destination configuration from the CF Space for the specified
	 * destination name
	 * 
	 * @param destinationName
	 * @return {@link ConnectionAttributes} if destination exists otherwise returns
	 *         null
	 */
	public ConnectionAttributes getDestinationAtSpaceByName(String destinationName) {

		LOGGER.info("[ENTER]" + this.getClass().getName() + " - getDestinationAtSpaceByName()");
		/* Read Destination Credentials */
		JSONObject destinationCredentials = null;
		String clientId = null, clientSecret = null;
		try {
			destinationCredentials = EnvironmentVariableAccessor.getServiceCredentials(DESTINATION_SERVICE_NAME,
					DESTINATION_INSTANCE);
			clientId = destinationCredentials.getString(SharedConstants.CLIENT_ID_ATTR);
			clientSecret = destinationCredentials.getString(SharedConstants.CLIENT_SECRET_ATTR);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		/* Read XSUAA URL */
		URI xsuaaUrl = null;
		try {
			xsuaaUrl = new URI(EnvironmentVariableAccessor.getServiceCredentialsAttribute(DESTINATON_XSUAA_SERVICE_NAME,
					DESTINATON_XSUAA_INSTANCE, "url"));
			// Destination Repository
			XsuaaDefaultEndpoints xsUaaEndpoint = new XsuaaDefaultEndpoints(xsuaaUrl);
			ClientCredentials clientCred = new ClientCredentials(clientId, clientSecret);
			// JWT Token for Destination Service
			String jwtToken = tokenFactory.getJWTWithClientCredentials(xsUaaEndpoint, clientCred);
			// HTTP Call to destination service
			ConnectionAttributes connectionAttributes = getDestination(destinationName, jwtToken);
			LOGGER.info("[EXIT]" + this.getClass().getName() + " - getDestinationAtSpaceByName()");
			return connectionAttributes;
		} catch (URISyntaxException | JSONException | MalformedURLException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Helper Method to get {@link ConnectionAttributes} for a given destination
	 * name
	 * 
	 * @param destinationName
	 * @param accessToken
	 * @return {@link ConnectionAttributes}
	 * @throws JSONException
	 * @throws MalformedURLException
	 */
	private ConnectionAttributes getDestination(String destinationName, String accessToken)
			throws JSONException, MalformedURLException {
		String destinationServiceUri = EnvironmentVariableAccessor
				.getServiceCredentialsAttribute(DESTINATION_SERVICE_NAME, DESTINATION_SERVICE_PROPTY_URI);

		/*
		 * Uncomment below to use local environment setup to access destinations
		 */

		// String destinationServiceUri =
		// "https://destination-configuration.cfapps.us10.hana.ondemand.com";
		LOGGER.info("Will get destination [" + destinationName + "], uri [" + destinationServiceUri
				+ "] with access token [" + accessToken + "]");

		String destinationPath = String.format(DESTINATION_SERVICE_PATH, destinationName);
		URL destinationUrl = new URL(destinationServiceUri + destinationPath);

		HttpURLConnection urlConnection = null;
		try {
			urlConnection = HttpUtils.openUrlConnection(destinationUrl);
			urlConnection.setRequestProperty(SharedConstants.HEADER_AUTORIZATION,
					SharedConstants.BEARER_WITH_TRAILING_SPACE + accessToken);

			HttpResponse response = HttpUtils.getResponse(urlConnection);

			if (response.getResponseCode() == HTTP_NOT_FOUND) {
				throw new DestinationNotFoundException("Destination '" + destinationName + "' could not be found");
			} else {
				return ConnectionAttributes.fromDestination(response.getResponseStream());
			}

		} catch (Exception e) {
			String msg = "EXCEPTION: " + e.getMessage() + ", desitnationName [" + destinationName
					+ "], destination URL [" + destinationUrl + "]";
			LOGGER.error(msg, e);
			throw new IllegalStateException(e);
		} finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
		}
	}

}
